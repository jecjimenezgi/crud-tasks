let apiPut = (taskid,payload) => {
  var myHeaders = new Headers();
  myHeaders.append("Content-Type", "application/json");

  var raw = JSON.stringify(payload);

  var requestOptions = {
    method: "PUT",
    headers: myHeaders,
    body: raw,
    redirect: "follow",
  };

  return fetch("https://task-api02.herokuapp.com/tasks/" + taskid, requestOptions)
};

export default apiPut;
