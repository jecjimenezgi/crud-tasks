let apiDelete = (taskid) => {
    var requestOptions = {
        method: 'DELETE',
        redirect: 'follow'
      };
      
    return fetch("https://task-api02.herokuapp.com/tasks/" + taskid, requestOptions)
 }

 export default apiDelete;