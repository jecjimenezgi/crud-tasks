let apiGet = () => {
    var requestOptions = {
        method: "GET",
        redirect: "follow",
      };
  
     return fetch("https://task-api02.herokuapp.com/tasks", requestOptions)
}

export default apiGet;