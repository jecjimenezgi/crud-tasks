import TodoContainer from './components/TodoContainer.js';
import './App.css';

function App() {
  return (
    <div className="App">
      <TodoContainer />
    </div>
  );
}

export default App;
