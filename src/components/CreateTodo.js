import "./CreateTodo.css";
import { useState, useEffect } from "react";
import apiPost from "../services/post.js";

let CreateTodo = ({ tasks, setTasks }) => {
  const [student, setStudent] = useState();
  const [task, setTask] = useState();

  let postEvent = (event) => {
    event.preventDefault();
    let form = event.target.parentElement.parentElement.parentElement;
    let student = form.querySelector('input[name="student"]');
    let task = form.querySelector('input[name="task"]');
    setStudent(student.value);
    setTask(task.value);
    task.value = "";
    student.value = "";
  };

  useEffect(() => {
    if (student && task) {
      let payload = { student: student, task: task };
      apiPost(payload)
        .then((respond) => respond.json())
        .then((_task) => {
          let new_tasks = [...tasks, _task];
          setStudent("");
          setTask("");
          setTasks(new_tasks);
        })
        .catch((e) => console.error(e));
    }
  }, [student, task, tasks, setTasks]);

  return (
    <div className="CreateTodo">
      <div>
        <h2>Create New Task</h2>
      </div>
      <form>
        <div className="row">
          <div className="col-5">
            <label>
              <h2>Student</h2>
            </label>
          </div>
          <div className="col-5">
            <label>
              <h2>Task</h2>
            </label>
          </div>
        </div>
        <div className="row">
          <div className="col-5">
            <input type="text" id="student" name="student"></input>
          </div>
          <div className="col-5">
            <input type="text" id="task" name="task"></input>
          </div>
        </div>
        <div className="row">
          <button type="submit" onClick={postEvent}>
            <h2>Create</h2>
          </button>
        </div>
      </form>
    </div>
  );
};

export default CreateTodo;
