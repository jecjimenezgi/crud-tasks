import "./TodoContainer.css";
import CreateTodo from "./CreateTodo.js";
import { useState, useEffect } from "react";
import apiGet from "../services/get.js";
import apiPut from "../services/put.js";
import apiDelete from "../services/delete.js";

let TodoContainer = () => {
  const [tasks, setTasks] = useState([]);
  const [tasksViews, setTasksViews] = useState(null);
  const [updateActive, setUpdateActive] = useState();
  const [updateCompleted, setUpdateCompleted] = useState();
  const [deleteTask, setDeleteTask] = useState();
  const [editMode, setEditMode] = useState();
  const [student, setStudent] = useState();
  const [task, setTask] = useState();
  const [writeContent, setWriteContent] = useState();

  let updateStateToActive = (event) => {
    let taskid = event.target.getAttribute("taskid");
    setUpdateActive(taskid);
  };

  let updateStateToCompleted = (event) => {
    let taskid = event.target.getAttribute("taskid");
    setUpdateCompleted(taskid);
  };

  let deleteTaksEvent = (event) => {
    let taskid = event.target.getAttribute("taskid");
    setDeleteTask(taskid);
  };

  let editEvent = (event) => {
    let taskid = event.target.getAttribute("taskid");
    setEditMode(taskid);
  };

  let studentEvent = (event) => {
    setStudent(event.target.value);
  };

  let writeEvent = (event) => {
    let taskid = event.target.getAttribute("taskid");
    setWriteContent(taskid);
  };

  let taskEvent = (event) => {
    setTask(event.target.value);
  };

  useEffect(() => {
    if (writeContent) {
      if (task && student) {
        let new_tasks = tasks.map((_task) => {
          if (_task.id.toString() === writeContent) {
            _task.student = student;
            _task.task = task;
            apiPut(_task.id, _task).catch((e) => console.error(e));
            return _task;
          } else {
            return _task;
          }
        });
        setWriteContent(0);
        setEditMode(0);
        setTasks(new_tasks);
      } else if (task) {
        let new_tasks = tasks.map((_task) => {
          if (_task.id.toString() === writeContent) {
            _task.task = task;
            apiPut(_task.id, _task).catch((e) => console.error(e));
            return _task;
          } else {
            return _task;
          }
        });
        setWriteContent(0);
        setEditMode(0);
        setTasks(new_tasks);
      } else if (student) {
        let new_tasks = tasks.map((_task) => {
          if (_task.id.toString() === writeContent) {
            _task.student = student;
            apiPut(_task.id, _task).catch((e) => console.error(e));
            return _task;
          } else {
            return _task;
          }
        });
        setWriteContent(0);
        setEditMode(0);
        setTasks(new_tasks);
      } else {
        setWriteContent(0);
        setEditMode(0);
      }
    }
  }, [writeContent, student, task, tasks]);

  useEffect(() => {
    if (deleteTask) {
      let new_tasks = tasks.filter((task) => {
        if (task.id.toString() === deleteTask) {
          apiDelete(task.id).catch((e) => console.error(e));
          return false;
        } else {
          return true;
        }
      });
      setDeleteTask(0);
      setTasks(new_tasks);
    }
  }, [deleteTask, tasks]);

  useEffect(() => {
    if (updateActive) {
      let new_tasks = tasks.map((task) => {
        if (task.id.toString() === updateActive) {
          task.state = false;
          apiPut(task.id, task).catch((e) => console.error(e));
          return task;
        } else {
          return task;
        }
      });
      setUpdateActive(0);
      setTasks(new_tasks);
    }
  }, [updateActive, tasks]);

  useEffect(() => {
    if (updateCompleted) {
      let new_tasks = tasks.map((task) => {
        if (task.id.toString() === updateCompleted.toString()) {
          task.state = true;
          apiPut(task.id, task).catch((e) => console.error(e));
          return task;
        } else {
          return task;
        }
      });
      setUpdateCompleted(0);
      setTasks(new_tasks);
    }
  }, [updateCompleted, tasks]);

  useEffect(() => {
    apiGet()
      .then((response) => response.json())
      .then((tasks) => setTasks(tasks))
      .catch((error) => console.log("error", error));
  }, []);

  useEffect(() => {
    let rows = tasks.map((task) => {
      let state = task.state ? (
        <span>
          <h2>&#10003;</h2>
        </span>
      ) : (
        <span>
          <h2>&#9999;</h2>
        </span>
      );
      let stateButton = task.state ? (
        <button onClick={updateStateToActive}>
          <span>
            <h2 taskid={task.id}>&#9999;</h2>
          </span>
        </button>
      ) : (
        <button onClick={updateStateToCompleted}>
          <span>
            <h2 taskid={task.id}>&#10003;</h2>
          </span>
        </button>
      );

      let typeContent = () => {
        if (!editMode) {
          return [
            <div key={task.id + "content-1"} className="col-4">
              <h2>{task.student}</h2>
            </div>,
            <div key={task.id + "content-2"} className="col-4">
              <h2>{task.task}</h2>
            </div>,
          ];
        } else {
          if (task.id.toString() === editMode) {
            return [
              <div key={task.id + "content-1"} className="col-4">
                <input onChange={studentEvent}></input>
              </div>,
              <div key={task.id + "content-2"} className="col-4">
                <input onChange={taskEvent}></input>
              </div>,
            ];
          } else {
            return [
              <div key={task.id + "content-1"} className="col-4">
                <h2>{task.student}</h2>
              </div>,
              <div key={task.id + "content-2"} className="col-4">
                <h2>{task.task}</h2>
              </div>,
            ];
          }
        }
      };

      let typeEditButton = () => {
        if (!editMode) {
          return (
            <button onClick={editEvent}>
              <span>
                <h2 taskid={task.id}>&#9997;</h2>
              </span>
            </button>
          );
        } else {
          if (task.id.toString() === editMode) {
            return (
              <button onClick={writeEvent}>
                <span>
                  <h2 taskid={task.id}>&#9989;</h2>
                </span>
              </button>
            );
          } else {
            return (
              <button onClick={editEvent}>
                <span>
                  <h2 taskid={task.id}>&#9997;</h2>
                </span>
              </button>
            );
          }
        }
      };

      let content = typeContent();
      let editButton = typeEditButton();

      return (
        <div key={task.id} className="row">
          {content}
          <div className="col-4">
            <div className="row">
              <div className="col-3">{state} </div>
              <div className="col-3">{stateButton}</div>
              <div className="col-3">{editButton}</div>
              <div className="col-3">
                <button onClick={deleteTaksEvent}>
                  <span>
                    <h2 taskid={task.id}>&#10060;</h2>
                  </span>
                </button>
              </div>
            </div>
          </div>
        </div>
      );
    });
    setTasksViews(rows);
  }, [tasks, editMode]);

  return (
    <div>
      <CreateTodo tasks={tasks} setTasks={setTasks}></CreateTodo>
      <div className="row">
        <div className="col-4">
          <label>
            <h2>Student</h2>
          </label>{" "}
        </div>
        <div className="col-4">
          <label>
            <h2>Task</h2>
          </label>{" "}
        </div>
        <div className="col-4">
          <div className="row">
            <div className="col-3">
              <label>
                <h2>Status</h2>
              </label>
            </div>
            <div className="col-3">
              <label>
                <h2>Update Status</h2>
              </label>
            </div>
            <div className="col-3">
              <label>
                <h2>Edit Values</h2>
              </label>
            </div>
            <div className="col-3">
              <label>
                <h2>Delete</h2>
              </label>
            </div>
          </div>
        </div>
      </div>
      <div className="taskViews">{tasksViews}</div>
    </div>
  );
};

export default TodoContainer;
